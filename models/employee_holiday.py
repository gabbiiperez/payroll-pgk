# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import timedelta
import logging

_logger = logging.getLogger(__name__)

class EmployeeHoliday(models.Model):
    _inherit = 'employee.holiday'


    cant_days_availability = fields.Selection(selection=lambda self: self._cant_days_availability(), string='Cantidad de dias disponibles', store=True, default='0')


    def _cant_days_availability(self):
        select = [('0','Seleccione una opcion..')]
        empl = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        if empl:
            hols = self.env['employee.holiday.item'].search([('employee_id', '=', empl.id)])
            cant_days = sum(hol_emp.days_available for hol_emp in hols)
            if cant_days <= 7:
                str_value = str(cant_days) + ' dias'
                select.append((int(cant_days), str_value))
            else:
                ammount = int(int(cant_days) / 7)
                cont = 0
                for i in range(ammount):
                    if cont < cant_days:
                        cont+= 7
                        str_cont = str(cont) + ' dias'
                        select.append((cont, str_cont))
        return select


    @api.onchange('date_inic')
    def _onchange_date_inic(self):
        if self.date_inic:
            cant_days = int(self.cant_days_availability)
            self.date_end = self.date_inic + timedelta(days=cant_days)
