# -*- coding: utf-8 -*-
{
    'name': 'Payroll Pgk',
    'summary': '',
    'author': 'Calyx Servicios S.A.',
    'maintainers': ['GabrielaPerez'],
    'category': 'Employee',
    'depends': ['base', 'employee_salary'],

    'data': [
        'views/employee_holiday_views.xml',
    ],
    'application': False,
    'installable': True,
}
